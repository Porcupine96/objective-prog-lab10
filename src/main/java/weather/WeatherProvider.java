package weather;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class WeatherProvider {

    private final String weatherApiUrl;

    public WeatherProvider(String weatherApiUrl) {
        this.weatherApiUrl = weatherApiUrl;
    }

    public Weather getWeather() throws WeatherServiceException {
        try (InputStream in = new URL(weatherApiUrl).openConnection().getInputStream()) {

            String response = IOUtils.toString(in, "UTF-8");
            JSONObject weather = new JSONObject(response).getJSONObject("main");
            return new Weather(weather.getDouble("temp"), weather.getInt("pressure"), weather.getInt("humidity"));

        } catch (IOException | JSONException ex) {
            throw new WeatherServiceException("Could not receive weather information", ex);
        }
    }

    public static class Weather {
        public final double temperature;
        public final int pressure;
        public final int humidity;

        Weather(double temperature, int pressure, int humidity) {
            this.temperature = temperature;
            this.pressure = pressure;
            this.humidity = humidity;
        }

        @Override
        public String toString() {
            return "Weather{" +
                    "temperature=" + temperature +
                    ", pressure=" + pressure +
                    ", humidity=" + humidity +
                    '}';
        }
    }

}

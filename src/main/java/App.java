import chat.Chat;
import chat.ChatWebSocketHandler;
import chat.ChatbotChannel;
import chat.SimpleChat;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import weather.WeatherProvider;

import static spark.Spark.*;

public class App {

    public static void main(String[] args) {

        Config config = ConfigFactory.load();
        WeatherProvider weatherProvider = setupWeatherService(config);
        ChatbotChannel chatbot = new ChatbotChannel(weatherProvider);
        Chat chat = new SimpleChat(chatbot);
        ChatWebSocketHandler webSocketHandler = new ChatWebSocketHandler(chat);

        setupWebSocket(webSocketHandler);
    }

    private static void setupWebSocket(ChatWebSocketHandler webSocketHandler) {
        staticFiles.location("/public");
        staticFiles.expireTime(600);
        webSocket("/chat", webSocketHandler);
        init();
    }

    private static WeatherProvider setupWeatherService(Config config) {
        String host = config.getString("weather.host");
        String params = config.getString("weather.params");
        String token = config.getString("weather.token");

        String weatherApiUrl = host + params + "&appId=" + token;

        return new WeatherProvider(weatherApiUrl);
    }

}

package chat;

import org.eclipse.jetty.websocket.api.Session;

public interface Chat {

    void handleUserJoin(Session user);

    void handleUserLeave(Session user);

    void handleMessage(Session user, String message);

}

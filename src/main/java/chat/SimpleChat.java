package chat;

import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import weather.WeatherProvider;

import java.net.HttpCookie;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class SimpleChat implements Chat {

    private Logger logger = LoggerFactory.getLogger(SimpleChat.class);

    private Map<Session, String> userSessionToUsername = new ConcurrentHashMap<>();
    private Map<Session, Channel> userSessionToChannel = new ConcurrentHashMap<>();
    private Map<String, Channel> channelNameToChannel = new ConcurrentHashMap<>();

    public SimpleChat(ChatbotChannel chatbot) {
        channelNameToChannel.put(chatbot.getName(), chatbot);
    }

    @Override
    public void handleUserJoin(Session user) {
        String username = getUsername(user);
        logger.info("User " + username + " has joined the chat.");
        userSessionToUsername.put(user, username);
        sendMessageToUser(user);
    }

    @Override
    public void handleUserLeave(Session user) {
        logger.info("User " + userSessionToUsername.get(user) + " has left the chat.");
        Channel channel = userSessionToChannel.get(user);
        channel.removeUser(user);
        channel.broadcastMessageOnChannel("Server", "User: " + userSessionToUsername.get(user) + " left the channel");
        userSessionToChannel.remove(user);
        sendMessageToUser(user);
    }

    @Override
    public void handleMessage(Session user, String message) {
        logger.info("Received message: " + message);
        if (message.startsWith("/message:")) broadcastUsersMessage(user, messageValue(message));
        else if (message.startsWith("/join-channel:")) joinChannel(user, messageValue(message));
        else if (message.startsWith("/create-channel:")) createChannel(user, messageValue(message));
        else if (message.startsWith("/leave-channel:")) handleUserLeave(user);
        else if (message.startsWith("/remove-channel:")) removeChannel(messageValue(message));
    }

    private void sendMessageToUser(Session user) {
        try {
            if (user.isOpen()) user.getRemote().sendString(String.valueOf(new JSONObject()
                    .put("type", "user")
                    .put("channellist", channelNameToChannel.keySet())
                    .put("userlist", userSessionToUsername.values())
            ));
        } catch (Exception ex) {
            logger.error("An exception occurred while sending a message to user", ex);
        }
    }

    private String getUsername(Session user) {
        return user
                .getUpgradeRequest()
                .getCookies()
                .stream()
                .filter(p -> p.getName().equals("username"))
                .findFirst()
                .map(HttpCookie::getValue)
                .orElse("");
    }

    private String messageValue(String message) {
        return message.substring(message.indexOf(":") + 1);
    }

    private void removeChannel(String channelName) {
        if (channelNameToChannel.get(channelName).isEmpty()) {
            channelNameToChannel.remove(channelName);
            lobbyBroadcast();
        }
    }

    private void broadcastUsersMessage(Session user, String messageContent) {
        userSessionToChannel
                .get(user)
                .broadcastMessageOnChannel(userSessionToUsername.get(user), messageContent);
    }

    private void joinChannel(Session user, String channelName) {
        Channel channel = channelNameToChannel.get(channelName);
        channel.addUser(user, getUsername(user));
        channel.broadcastMessageOnChannel("Server", "User: " + userSessionToUsername.get(user) + " joined the channel");
        userSessionToChannel.put(user, channelNameToChannel.get(channelName));
    }

    private void createChannel(Session user, String channelName) {
        if (!channelNameToChannel.containsKey(channelName))
            channelNameToChannel.put(channelName, new Channel(channelName));
        sendMessageToUser(user);
        lobbyBroadcast();
    }

    private void lobbyBroadcast() {
        broadcastToUsers(usersInLobby());
    }

    private void broadcastToUsers(List<Session> users) {
        users.forEach(this::sendMessageToUser);
    }

    private List<Session> usersInLobby() {
        return userSessionToUsername.keySet().stream().filter(e -> !userSessionToChannel.containsKey(e)).collect(Collectors.toList());
    }

}

package chat;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

@WebSocket
public class ChatWebSocketHandler {

    private final Chat chat;

    public ChatWebSocketHandler(Chat chat) {this.chat = chat;}

    @OnWebSocketConnect
    public void onConnect(Session user) throws Exception {
        chat.handleUserJoin(user);
    }

    @OnWebSocketClose
    public void onClose(Session user, int statusCode, String reason) {
        chat.handleUserLeave(user);
    }

    @OnWebSocketMessage
    public void onMessage(Session user, String message) {
        chat.handleMessage(user, message);
    }

}
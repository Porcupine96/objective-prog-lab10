package chat;

import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static j2html.TagCreator.*;

public class Channel {

    private Logger logger = LoggerFactory.getLogger(Channel.class);

    private Map<Session, String> userSessionToUsername = new ConcurrentHashMap<>();
    private String name;

    public Channel(String name) {
        this.name = name;
    }

    void addUser(Session user, String username) {
        userSessionToUsername.put(user, username);
    }

    void removeUser(Session user) {
        userSessionToUsername.remove(user);
    }

    boolean isEmpty() { return userSessionToUsername.isEmpty(); }

    void broadcastMessageOnChannel(String sender, String message) {
        userSessionToUsername.keySet().stream().filter(Session::isOpen).forEach(session -> {
            try {
                session.getRemote().sendString(String.valueOf(new JSONObject()
                        .put("type", "channel")
                        .put("userMessage", createHtmlMessageFromSender(sender, message))
                        .put("userlist", userSessionToUsername.values())
                ));
            } catch (JSONException | IOException ex) {
                logger.error("An exception occurred while sending a message on channel: " + name, ex);
            }
        });
    }

    private String createHtmlMessageFromSender(String sender, String message) {
        return article().with(
                b(sender + " says:"),
                p(message),
                span().withClass("timestamp").withText(new SimpleDateFormat("HH:mm:ss").format(new Date()))
        ).render();
    }

    String getName() {
        return name;
    }

}

package chat;

import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import weather.WeatherProvider;
import weather.WeatherServiceException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ChatbotChannel extends Channel {

    private final Logger logger = LoggerFactory.getLogger(ChatbotChannel.class);

    private final WeatherProvider weatherProvider;

    public ChatbotChannel(WeatherProvider weatherProvider) {
        super("chatbot");
        this.weatherProvider = weatherProvider;
    }

    @Override
    void broadcastMessageOnChannel(String sender, String message) {
        super.broadcastMessageOnChannel(sender, message);
        switch (message) {
            case "What's the weather like in Cracow?":
                respondWithWeather();
                break;
            case "What day is today?":
                respondWithWeekday();
                break;
            case "What time is it?":
                respondWithTime();
                break;
        }
    }

    private void respondWithTime() {
        String formattedTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("hh:mm:ss"));
        super.broadcastMessageOnChannel(getName(), "It's " + formattedTime + ".");
    }

    private void respondWithWeekday() {
        String weekday = WordUtils.capitalizeFully(LocalDateTime.now().getDayOfWeek().toString());
        super.broadcastMessageOnChannel(getName(), "It's " + weekday + ".");
    }

    private void respondWithWeather() {
        try {

            WeatherProvider.Weather weather = weatherProvider.getWeather();
            String weatherMessage = "The temperature is " +
                    (int) (weather.temperature - 273.15) + "°C, " +
                    "humidity: " + weather.humidity + "%" +
                    " and the air pressure: " + weather.pressure + "Pa.";
            super.broadcastMessageOnChannel(getName(), weatherMessage);

        } catch (WeatherServiceException ex) {
            logger.error("Error during weather request", ex);
            super.broadcastMessageOnChannel(getName(), "Sorry, there was a problem with the weather service!");
        }
    }

}

var webSocket = new WebSocket("ws://" + location.hostname + ":" + location.port + "/chat/");

webSocket.onmessage = function (msg) {
    updateChat(msg);
};

webSocket.onclose = function () {
    alert("WebSocket connection closed");
    location.href = "/"
};

id("send").addEventListener("click", function () {
    sendMessage("/message:" + id("message").value);
});

id("leave").addEventListener("click", function () {
    sendMessage("/leave-channel:");
    clearSection("chat");
    id("channel").style.display = "none";
    id("channelselection").style.display = "block";
});

id("message").addEventListener("keypress", function (key) {
    if (key.keyCode === 13) sendMessage("/message:" + key.target.value);
});

id("create").addEventListener("click", function () {
    if (nonEmpty("newchannel")) {
        webSocket.send("/create-channel:" + id("newchannel").value);
        clearSection("newchannel");
    }
});

id("newchannel").addEventListener("keypress", function (e) {
    if (e.keyCode === 13 && nonEmpty("newchannel")) {
        webSocket.send("/create-channel:" + e.target.value);
        clearSection("newchannel");
    }
});

function sendMessage(message) {
    if (message !== "") {
        webSocket.send(message);
        clearSection("message");
    }
}

function updateChat(msg) {
    var data = JSON.parse(msg.data);
    if (data.type === "channel") {
        insert("chat", data.userMessage);
        updateUserList(data)
    }
    else {
        updateChannelList(data)
    }
}

function updateUserList(data) {
    clearSection("userlist");
    data.userlist.forEach(function (user) {
        insert("userlist", "<li>" + user + "</li>");
    });
}

function updateChannelList(data) {
    clearSection("channellist");
    data.channellist.forEach(function (channel) {
        insert("channellist", '<li><button onclick="removeChannel(' + "'" + channel + "'" + ')">' + "X" + '</button><button onclick="joinChannel(' + "'" + channel + "'" + ')">' + channel + '</button></li>');
    });
}

function joinChannel(channelName) {
    webSocket.send("/join-channel:" + channelName);
    id("channelselection").style.display = "none";
    id("channel").style.display = "block"
}

function removeChannel(channelName) {
    webSocket.send("/remove-channel:" + channelName);
}


/* utility methods */

function insert(targetId, message) {
    id(targetId).insertAdjacentHTML("afterbegin", message);
}

function clearSection(sectionName) {
    id(sectionName).innerHTML = "";
}

function id(id) {
    return document.getElementById(id);
}

function nonEmpty(textBoxName) {
    return id(textBoxName).value != "";
}
name := "objective-prog-lab10"

version := "1.0"

libraryDependencies += "com.sparkjava" % "spark-core" % "2.5.4"
libraryDependencies += "com.j2html" % "j2html" % "0.7"

libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.22"
libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.22"

libraryDependencies += "com.typesafe" % "config" % "1.3.1"

libraryDependencies += "commons-io" % "commons-io" % "2.5"

libraryDependencies += "org.springframework" % "spring-context" % "4.3.5.RELEASE"


